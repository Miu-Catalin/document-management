from django.urls import path, include
from rest_framework import routers
from document.views import ListCreateDocumentView, ListCreatePackageView, UpdateDocumentStatusView

router = routers.DefaultRouter()
router.register('packages', ListCreatePackageView)
router.register('documents', ListCreateDocumentView)
router.register('document-detail', UpdateDocumentStatusView, base_name = 'document-detail')

# Wire up our API using automatic URL routing.

urlpatterns = [
    path('', include(router.urls)),
    ]