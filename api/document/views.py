from django.shortcuts import render, get_object_or_404
import datetime
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateAPIView
from rest_framework.viewsets import ViewSetMixin
from document.models import (Package, Document)
# from rest_framework import viewsets
# from rest_framework.decorators import detail_route
from document.serializers import (PackageSerializer, DocumentSerializer, DocumentStatusSerializer)

# Create your views here.
class ListCreatePackageView(ViewSetMixin, ListCreateAPIView):
    """
    API endpoint that allows packages to be viewed or edited.
    """
    queryset = Package.objects.all().order_by('pack_id')
    serializer_class = PackageSerializer

class ListCreateDocumentView(ViewSetMixin, ListCreateAPIView):
    """
    API endpoint that allows documents to be viewed or created.
    """
    queryset = Document.objects.all().order_by('doc_id')
    serializer_class = DocumentSerializer
    
    def perform_create(self, serializer):
        return serializer.save(created_by=self.request.user,
                                status_updated_by = self.request.user)
    
class UpdateDocumentStatusView(ViewSetMixin, RetrieveUpdateAPIView):
    """
    API endpoint that allows viewing document detail and updating the status of a document.
    """
    serializer_class = DocumentStatusSerializer
    lookup_field = 'doc_id'
    # lookup_url_kwarg = 'doc_id'
    
    def get_object(self):
        doc_id = self.kwargs['doc_id']
        document = get_object_or_404(Document, doc_id = doc_id)
        return document
    
    # def perform_update(self, serializer):
    #     return serializer.save(status = self.request.status,
    #                             status_updated_by = self.request.user,
    #                             status_updated_at = datetime.datetime.now())
        