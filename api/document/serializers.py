from document.models import (Package, Document)
from rest_framework import serializers

class PackageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Package
        fields = ('__all__')
        lookup_value = 'created_by'
        documents = serializers.PrimaryKeyRelatedField(many=True, read_only = True)

class DocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
        fields = ('doc_id','doc_type', 'package', 'comments', 'owner', 'created_at', 'created_by', 'status', 'status_updated_at', 'status_updated_by')
        read_only_fields = ('created_at', 'created_by', 'status', 'status_updated_at', 'status_updated_by')
        lookup_value = 'doc_id'

class DocumentStatusSerializer(serializers.Serializer):
    class Meta:
        model = Document
        fields = ('doc_id', 'status', 'status_updated_at', 'status_updated_by')
        read_only_fields = ('status_updated_at', 'status_updated_by')
        lookup_value = 'doc_id'
