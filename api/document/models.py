import uuid
from django.db import models
from datetime import datetime
from api.settings import (ORG_USER_TYPES, DOC_STATUSES, DOC_TYPES, PACKAGE_TYPES)
from api.settings import AUTH_USER_MODEL as User
from user.models import User

# Create your models here.

# Document package model to have a 1-to-1 relationship with org client user and 1-to-many relationship with documents
class Package(models.Model):
    pack_id = models.UUIDField(primary_key = True, default = uuid.uuid4, editable = False)
    pack_type = models.CharField(max_length = 2, choices = PACKAGE_TYPES)
    owner = models.ForeignKey(User, on_delete = models.CASCADE, limit_choices_to = {'org_role': 3}, related_name = 'client_packages')
    # description = models.TextField()
    
    def __str__(self):
        return self.pack_type

#  Document model
class Document(models.Model):
    doc_id = models.UUIDField(primary_key = True, default = uuid.uuid4, editable=False)
    
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User, editable = False, on_delete = models.CASCADE, related_name = 'documents_created_by')

        
    status_updated_at = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length = 2, choices = DOC_STATUSES, default = DOC_STATUSES[0])
    status_updated_by = models.ForeignKey(User, editable = False, blank = True, on_delete = models.CASCADE, related_name = 'statuses_updated_by')
    
    doc_type = models.CharField(max_length = 2, choices = DOC_TYPES)
    
    owner = models.ForeignKey(User, on_delete = models.CASCADE, related_name = 'client_documents')
    
    package = models.ManyToManyField(Package)
    
    comments = models.TextField(default = '')